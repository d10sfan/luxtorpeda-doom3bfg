#!/bin/bash

source env.sh

set -x
set -e

log_environment
prepare_dist_dirs "$STEAM_APP_ID_LIST"

# build doom3bdg
#
pushd "source"
cd neo
./cmake-eclipse-linux-profile.sh
cd ../build
make -j "$(nproc)"
popd

cp -rfv "source/build/RBDoom3BFG" "208200/dist/RBDoom3BFG"
